package sk.murin.rock_scissors_paper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Game implements ActionListener {
    private int computerWon;
    private int playerWon;
    private JLabel labelIconMiddle;
    private JLabel labelPlayerWon;
    private JLabel labelScore;
    private final Font f = new Font("Consolas", Font.BOLD, 17);
    private final ImageIcon[] poleIcon = {new ImageIcon("paper.png"), new
            ImageIcon("scissors.png"), new ImageIcon("rock.png")};
    private final String[] names = {"paper", "scissors", "rock"};
    private final Random r = new Random();

    public Game() {
        setup();
    }

    private void setup() {
        JFrame frame = new JFrame("Nesed za tym cele dni !");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 400);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setBackground(Color.GRAY);
        frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        frame.add(makePanelTop(), BorderLayout.NORTH);
        frame.add(makePanelCenter(), BorderLayout.CENTER);
        frame.add(makePanelBottom(), BorderLayout.SOUTH);
        frame.setVisible(true);
    }

    private JPanel makePanelTop() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 2));
        labelPlayerWon = new JLabel();
        labelPlayerWon.setText("Vyhral " );
        labelScore = new JLabel();
        labelScore.setText("PC : " + computerWon + " Hráč : " + playerWon);

        labelPlayerWon.setHorizontalAlignment(SwingConstants.CENTER);
        labelPlayerWon.setLocation(15, 2);
        labelPlayerWon.setFont(f);
        panel.add(labelPlayerWon);

        labelScore.setHorizontalAlignment(SwingConstants.LEADING);
        labelScore.setFont(f);
        panel.add(labelScore);

        return panel;
    }

    private JPanel makePanelCenter() {
        JPanel panel = new JPanel();
        labelIconMiddle = new JLabel();
        labelIconMiddle.setHorizontalAlignment(SwingConstants.CENTER);
        labelIconMiddle.setVerticalAlignment(SwingConstants.CENTER);

        ImageIcon imgMiddle = new ImageIcon(new ImageIcon("paper.png").getImage().getScaledInstance(300, 200, Image.SCALE_DEFAULT));
        labelIconMiddle.setIcon(imgMiddle);

        panel.add(labelIconMiddle);
        return panel;
    }

    private JPanel makePanelBottom() {
        JPanel panel = new JPanel();
        JButton[] buttons = new JButton[3];
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton();
            buttons[i].setPreferredSize(new Dimension(130, 60));
            buttons[i].setFocusable(false);

            poleIcon[i] = getScaledImage(poleIcon[i], buttons[i].getWidth() + 100, buttons[i].getHeight() + 75);
            buttons[i].setIcon(poleIcon[i]);
            buttons[i].setName(names[i]);
            panel.add(buttons[i]);
            buttons[i].addActionListener(this);
        }
        return panel;
    }

    private static ImageIcon getScaledImage(ImageIcon srcImg, int w, int h) {
        Image image = srcImg.getImage();
        Image newimg = image.getScaledInstance(w, h, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        return new ImageIcon(newimg);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton buttonPressed = (JButton) e.getSource();
        String pressedIconName = buttonPressed.getName();
        int randomPosition = r.nextInt(poleIcon.length);

        labelIconMiddle.setIcon(new ImageIcon(poleIcon[randomPosition].getImage().getScaledInstance(300, 200, Image.SCALE_DEFAULT)));

        String winner;
        if (pressedIconName.equals("rock") && names[randomPosition].equals("paper")) {
            winner = "Počítač ";
            computerWon++;
        } else if (pressedIconName.equals("paper") && names[randomPosition].equals("rock")) {
            winner = "Hráč ";
            playerWon++;
        } else if (pressedIconName.equals("scissors") && names[randomPosition].equals("rock")) {
            winner = "Počítač ";
            computerWon++;
        } else if (pressedIconName.equals("rock") && names[randomPosition].equals("scissors")) {
            winner = "Hráč ";
            playerWon++;
        } else if (pressedIconName.equals("scissors") && names[randomPosition].equals("paper")) {
            winner = "Hráč ";
            playerWon++;
        } else if (pressedIconName.equals("paper") && names[randomPosition].equals("scissors")) {
            winner = "Počítač ";
            computerWon++;

        } else {
            winner = "Remiza ";
        }
        labelPlayerWon.setText("Vyhral : " + winner);
        labelScore.setText("PC : " + computerWon + " Hráč : " + playerWon);
    }
}

